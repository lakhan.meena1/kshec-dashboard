module ApplicationHelper
	
  def is_same_route?(url)
  	if request.env['PATH_INFO'].include?(url)
  		return "nav-link active"
  	else
  		return "nav-link"
  	end
  end

  def designation_collection
    Designation.order(position: :asc).pluck(:name, :id)
  end

  def department_collection
    DepartmentList.order(position: :asc).pluck(:name, :id)
  end

  def program_collection
    Program.order(position: :asc).pluck(:name, :id)
  end

  def subject_collection
    Subject.order(name: :asc).pluck(:name, :id)
  end

  def faculty_designation_collection
    FacultyDesignation.order(position: :asc).pluck(:name, :id)
  end

  def infrastructure_collection
    Infrastructure.pluck(:campus, :id)
  end

end
