module InfrastructuresHelper
	def campus_collection
		[
			["Main Campus", "main campus"],
			["Constituent College", "constituent college"],
			["Off Campus", "off campus"],
			["Out Reach", "out reach"]
		]
	end

	def hostel_type_collection
		[
			["Boy's Hostel", "boy's hostel"],
			["Girl's Hostel", "girl's hostel"]
		]
	end

	def ict_type_collection
		[
			["WiFi", "wifi"],
			["Broadband", "broadband"]
		]
	end

	def availability_collection
		[
			["Teachers", "teachers"],
			["Students", "students"],
			["Both", "both"]
		]
	end

end
