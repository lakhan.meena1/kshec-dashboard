module DepartmentsHelper
	def category_collection
		["GM", "SC", "ST", "Cat-I", "Cat-II", "OBC", "Other"]
	end

	def sanctioned_collection
		["Government", "Management"]
	end

	def admitted_collection
		["Year", "Government", "Management"]
	end

end
