class AuthoritiesController < ApplicationController
  before_action :set_authority, only: %i[ show edit update destroy ]

  def index
    @authorities = Authority.includes(:designation).order('designations.position ASC')
  end

  def show
  end

  def new
    @authority = Authority.new
  end

  def edit
  end

  def create
    @authority = Authority.new(authority_params)
    respond_to do |format|
      if @authority.save
        format.html { redirect_to authorities_url, notice: "Authority was successfully created." }
        format.json { render :show, status: :created, location: @authority }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @authority.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @authority.update(authority_params)
        format.html { redirect_to authorities_url, notice: "Authority was successfully updated." }
        format.json { render :show, status: :ok, location: @authority }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @authority.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @authority.destroy
    respond_to do |format|
      format.html { redirect_to authorities_url, notice: "Authority was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    def set_authority
      @authority = Authority.find_by(id: params[:id])
    end

    def authority_params
      params.require(:authority).permit(:name, :qualification, :date_of_appointment, :email, :contact_number, :designation_id, :upload_list)
    end
end
