class BasicInformationsController < ApplicationController
  before_action :set_basic_information, only: %i[ show edit update destroy ]

  def index
    if params[:name].present?
      @basic_informations = BasicInformation.search_by_name(params[:name])
    else
      @basic_informations = BasicInformation.order(created_at: :desc)
    end
  end

  def show
  end

  def new
    @basic_information = BasicInformation.new
    @basic_information.build_naac_status
  end

  def edit
  end

  def create
    @basic_information = BasicInformation.new(basic_information_params)

    respond_to do |format|
      if @basic_information.save
        # update_students_faculties_count(@basic_information)
        format.html { redirect_to basic_informations_url, notice: "Basic information was successfully created." }
        format.json { render :show, status: :created, location: @basic_information }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @basic_information.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @basic_information.update(basic_information_params)
        # update_students_faculties_count(@basic_information)
        format.html { redirect_to basic_informations_url, notice: "Basic information was successfully updated." }
        format.json { render :show, status: :ok, location: @basic_information }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @basic_information.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @basic_information.destroy

    respond_to do |format|
      format.html { redirect_to basic_informations_url, notice: "Basic information was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def update_students_faculties_count(info)
    if info.programmes_offereds.present?
      f_count = 0
      s_count = 0
      info.programmes_offereds.each do |pa|
        f_count += pa.department_list.faculties.count
        pa.department_list.students.each do |s|
          s_count += (s.male + s.female + s.physical_challenged + s.third_gender + s.within_state + s.outside_state + s.foreign_student)  
        end
      end
      info.update(total_faculties: f_count, total_stduents: s_count)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_information
      @basic_information = BasicInformation.find_by(id: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def basic_information_params
      params.require(:basic_information).permit(:university_name, :date_of_establishment, :date_of_operationalization, :nirf_rank, :address,
        naac_status_attributes: [:id, :grade, :valid_from, :valid_upto],
        affiliated_colleges_attributes: [:id, :infrastructure_id, :_destroy],
        programmes_offereds_attributes: [:id, :department_list_id, :program_id, :_destroy]      )
    end
end
