class InfrastructuresController < ApplicationController
  before_action :set_infrastructure, only: %i[ show edit update destroy ]

  def index
    @infrastructures = Infrastructure.order(campus: :asc)
    respond_to do |format|
      format.html
      format.js
      format.csv { send_data Infrastructure.to_csv(@infrastructures), filename: "infrastructure.csv" }
    end
  end

  def show
  end

  def new
    @infrastructure = Infrastructure.new
    2.times { @infrastructure.hostels.build }
  end

  def edit
  end

  def create
    @infrastructure = Infrastructure.new(infrastructure_params)
    respond_to do |format|
      if @infrastructure.save
        format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully created." }
        format.json { render :show, status: :created, location: @infrastructure }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @infrastructure.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @infrastructure.update(infrastructure_params)
        format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully updated." }
        format.json { render :show, status: :ok, location: @infrastructure }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @infrastructure.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @infrastructure.destroy
    respond_to do |format|
      format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_infrastructure
      @infrastructure = Infrastructure.find_by(id: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def infrastructure_params
      params.require(:infrastructure).permit(:name, :campus, :land_availability, :no_of_classes, :no_of_labs,
        hostels_attributes: [:id, :hostel_type, :name,
            :total_boys, :total_girls, :_destroy],
        libraries_attributes: [:id, :name,
            :total_books, :total_ebooks, :total_journals, :total_ejournals, :_destroy],
        ict_infrastructures_attributes: [:id, :ict_type, :availability, :no_of_ict_enabled_classes, :speed, :service_provider, :bandwidth, :_destroy]
      )
    end
end
