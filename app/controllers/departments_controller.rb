class DepartmentsController < ApplicationController
  before_action :set_department, only: %i[ show edit update destroy ]

  def index
    @departments = Department.order(created_at: :desc)
  end

  def show
  end

  def new
    @department = Department.new
    @department.build_fee_structure
    @student = @department.build_student
  end

  def edit
  end

  def create
    @department = Department.new(department_params)

    respond_to do |format|
      if @department.save
        format.html { redirect_to departments_url, notice: "Department was successfully created." }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to departments_url, notice: "Department was successfully updated." }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to departments_url, notice: "Department was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def get_subjects
    program = Program.find_by(name: params[:name])
    @subjects = program.subjects.pluck(:name, :id) if program.present?
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find_by(id: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def department_params
      params.require(:department).permit(:program_id, :subject_id, :department_list_id,
        faculties_attributes: [:id, :name, :faculty_designation_id, :total_yrs_of_exp, :qualification, :resume, :date_of_joining, :_destroy],
        
        student_attributes: [:id, :category, :total_stduents, :male, :female, :physical_challenged, :third_gender, :within_state, :outside_state, :foreign_student, :sanctioned_intake_mgmt, :sanctioned_intake_govt, :admitted_mgmt, :admitted_year, :admitted_govt, :gm_seat, :sc_seat, :st_seat, :cat_1_seat, :cat_2_seat, :obc_seat, :other_seat, sanctioned_intakes: [], admitteds: [], categories: []],
        
        fee_structure_attributes: [:id, :management_fee, :govt_fee]   
      )
    end
end
