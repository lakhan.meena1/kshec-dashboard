class IctInfrastructure < ApplicationRecord
	validates :ict_type, presence: true
	enum ict_type: {"wifi": 0, "broadband": 1}
	enum availability: {"teachers": 0, "students": 1, "both": 2}
	belongs_to :infrastructure
end
