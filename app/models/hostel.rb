class Hostel < ApplicationRecord
	# validates :hostel_type, presence: true
	enum hostel_type: {"boy's hostel": 0, "girl's hostel": 1}
	belongs_to :infrastructure
end
