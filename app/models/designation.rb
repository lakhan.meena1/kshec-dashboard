class Designation < ApplicationRecord
	validates :name, presence: true
	has_many :authorities
end
