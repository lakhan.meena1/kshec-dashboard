class Authority < ApplicationRecord
	belongs_to :designation
	validates :name, :designation_id, presence: true
	mount_uploader :upload_list, FileUploader
end
