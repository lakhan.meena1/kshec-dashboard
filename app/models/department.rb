class Department < ApplicationRecord
	validates :department_list_id, :program_id, presence: true
	belongs_to :program
	belongs_to :subject, optional: true
	belongs_to :department_list
	has_many :faculties
	has_one :student
	has_one :fee_structure
	accepts_nested_attributes_for :faculties, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :student, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :fee_structure, reject_if: :all_blank, allow_destroy: true

	def fetch_faculties
		if self.faculties.present?
			self.faculties.includes(:faculty_designation).order("faculty_designations.position ASC")
		else
			[]
		end
	end
end
