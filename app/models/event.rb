class Event < ApplicationRecord
	validates :event_file, :department_list_id, presence: true
	belongs_to :department_list
	mount_uploader :event_file, FileUploader
	enum semester: { even: 'even', odd: 'odd' }

end
