class Infrastructure < ApplicationRecord
	require 'csv'
	validates :campus, presence: true
	has_many :hostels, dependent: :destroy
	has_many :libraries, dependent: :destroy
	has_many :ict_infrastructures, dependent: :destroy

	enum campus: {"main campus": 0, "constituent college": 1, "off campus": 2, "out reach": 3}
	
	accepts_nested_attributes_for :hostels, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :libraries, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :ict_infrastructures, reject_if: :all_blank, allow_destroy: true

	def self.to_csv(infrastructures)
    #headers = ["Name", "Campus", "Land Availability (in acres)", "No of Classes", "No of Labs", "Hostel Name", "Hostel Type", "Total Boys", "Total Girls", "Library Name", "Total Books", "Total E-books", "Total Journals", "Total E-journals", "ICT Type", "Availability", "No of ICT Enabled Classes", "Speed", "Service Provide", "Bandwidth"]
     headers = ["Name", "Campus", "Land Availability (in acres)", "No of Classes", "No of Labs"]
    CSV.generate(headers: true) do |csv|
      csv << headers
      if infrastructures.present?
        infrastructures.each do |infra|
        	# hostels_attributes = []
   				# if infra.hostels.present?
   				# 	infra.hostels.each do |h|
   				# 		hostels_attributes << [
   				# 			h.name,
   				# 			h.hostel_type,
   				# 			h.total_boys,
   				# 			h.total_girls
   				# 		].flatten
   				# 	end
   				# end

   				# libraries_attributes = []
   				# if infra.libraries.present?
   				# 	infra.libraries.each do |l|
   				# 		hostels_attributes << [
   				# 			l.name,
   				# 			l.total_books,
   				# 			l.total_ebooks,
   				# 			l.total_journals,
   				# 			l.total_ejournals,
   				# 		].flatten
   				# 	end
   				# end

   				# ict_attributes = []
   				# if infra.ict_infrastructures.present?
   				# 	infra.ict_infrastructures.each do |i|
   				# 		hostels_attributes << [
   				# 			i.ict_type,
   				# 			i.availability,
   				# 			i.no_of_ict_enabled_classes,
   				# 			i.speed,
   				# 			i.service_provider,
   				# 			i.bandwidth
   				# 		].flatten
   				# 	end
   				# end

          csv << [
            infra.name,
            infra.campus,
            infra.land_availability,
            infra.no_of_classes,
            infra.no_of_labs
          ].flatten
        end

      end
    end
  end 

end
