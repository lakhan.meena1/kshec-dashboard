class Subject < ApplicationRecord
	validates :name, presence: true
	belongs_to :program
end
