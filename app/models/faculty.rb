class Faculty < ApplicationRecord
	belongs_to :faculty_designation
	belongs_to :department, optional: true
	mount_uploader :resume, FileUploader
end
