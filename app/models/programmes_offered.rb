class ProgrammesOffered < ApplicationRecord
	belongs_to :basic_information
	belongs_to :department_list
	belongs_to :program
end
