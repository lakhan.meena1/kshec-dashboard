class BasicInformation < ApplicationRecord
	include PgSearch::Model
 	pg_search_scope :search_by_name, against: :university_name

	validates :university_name, presence: true
	has_one :naac_status
	has_many :affiliated_colleges
	has_many :programmes_offereds
	accepts_nested_attributes_for :naac_status, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :affiliated_colleges, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :programmes_offereds, reject_if: :all_blank, allow_destroy: true

end
