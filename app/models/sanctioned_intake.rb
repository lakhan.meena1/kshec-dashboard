class SanctionedIntake < ApplicationRecord
	belongs_to :student
	enum intake_type: {"government": 0, "management": 1}
end
