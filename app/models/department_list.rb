class DepartmentList < ApplicationRecord
	has_many :departments
	has_many :faculties, through: :departments
	has_many :students, through: :departments
end
