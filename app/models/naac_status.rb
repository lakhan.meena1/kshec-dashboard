class NaacStatus < ApplicationRecord
	validates :grade, presence: true
	belongs_to :basic_information
end
