require "application_system_test_case"

class InfrastructuresTest < ApplicationSystemTestCase
  setup do
    @infrastructure = infrastructures(:one)
  end

  test "visiting the index" do
    visit infrastructures_url
    assert_selector "h1", text: "Infrastructures"
  end

  test "should create infrastructure" do
    visit infrastructures_url
    click_on "New infrastructure"

    click_on "Create Infrastructure"

    assert_text "Infrastructure was successfully created"
    click_on "Back"
  end

  test "should update Infrastructure" do
    visit infrastructure_url(@infrastructure)
    click_on "Edit this infrastructure", match: :first

    click_on "Update Infrastructure"

    assert_text "Infrastructure was successfully updated"
    click_on "Back"
  end

  test "should destroy Infrastructure" do
    visit infrastructure_url(@infrastructure)
    click_on "Destroy this infrastructure", match: :first

    assert_text "Infrastructure was successfully destroyed"
  end
end
