User.create!(name: "Admin Account", email: "admin@gmail.com", password: "admin@123", password_confirmation: "admin@123")


Designation.create(name: "Chancellor", position: 1)
Designation.create(name: "Pro Chancellor", position: 2)
Designation.create(name: "BoG’s", position: 3)
Designation.create(name: "Pro Vice Chancellor", position: 4)
Designation.create(name: "Vice Chancellor", position: 5)
Designation.create(name: "Registrar", position: 6)
Designation.create(name: "Contorller of Examination", position: 7)
Designation.create(name: "Finance Officer", position: 8)


DepartmentList.create(name: "UG", position: 1)
DepartmentList.create(name: "Hon. UG", position: 2)
DepartmentList.create(name: "Integrated", position: 3)
DepartmentList.create(name: "PG", position: 4)
DepartmentList.create(name: "PHD", position: 5)
DepartmentList.create(name: "PG Diploma", position: 6)
DepartmentList.create(name: "Diploma", position: 7)
DepartmentList.create(name: "Certificate", position: 8)


Program.create(name: "Humanities", position: 1)
Program.create(name: "Science", position: 2)
Program.create(name: "Commerce and Management", position: 3)
Program.create(name: "Languages", position: 4)

humanities_arr = ["Education", "Folklore", "Economics", "History", "Journalism & Mass Communication", "Philosophy", "Physical Education", "Political Science", "Public Administration", "Rural Development", "Social Work", "Sociology", "Womens Studies"]
humanities = Program.find_by(name: "Humanities")
humanities_arr.each do |n|
	Subject.create(name: n, program_id: humanities.id)
end

science_arr = ["Anthropology", "Bio-Chemistry", "Botany", "Chemistry", "Computer Science", "Criminology and Forensic Science", "Earth Science", "Electronics", "Environmental Science", "Food Science", "Food Technology", "Home Science", "Library & Information Science", "Mathematics", "Microbiology & Biotechnology", "Organic, Analytical & Industrial Chemistry", "Physics", "Psychology", "Sericulture", "Statistics", "Zoology & Genetics", "Fashion Technology/ Interior Design", "Hotel Management" , "Performing Arts", "Visual Arts", "Visual Communication", "Geography"]
science = Program.find_by(name: "Science")
science_arr.each do |n|
	Subject.create(name: n, program_id: science.id)
end


com_and_mang_arr = ["Commerce", "Management"]
c = Program.find_by(name: "Commerce and Management")
com_and_mang_arr.each do |n|
	Subject.create(name: n, program_id: c.id)
end


lang_arr = ["Arabic", "English", "French", "German", "Hindi", "Kannada", "Malayalam", "Marathi", "Persian", "Sanskrit", "Tamil", "Telugu", "Urdu"]
lang = Program.find_by(name: "Languages")
lang_arr.each do |n|
	Subject.create(name: n, program_id: lang.id)
end

FacultyDesignation.create(name: "Assistant Professor", position: 1)
FacultyDesignation.create(name: "Associate Professor", position: 2)
FacultyDesignation.create(name: "Professor", position: 3)
FacultyDesignation.create(name: "Professor of Practice", position: 4)
FacultyDesignation.create(name: "Emeritus / Adjunct Faculty", position: 5)
FacultyDesignation.create(name: "Hon. / Visiting faculty", position: 6)

