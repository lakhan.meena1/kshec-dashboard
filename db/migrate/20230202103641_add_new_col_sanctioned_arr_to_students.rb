class AddNewColSanctionedArrToStudents < ActiveRecord::Migration[7.0]
  def change
    remove_column :students, :sanctioned_intake, :integer
    remove_column :students, :admitted, :integer
    remove_column :students, :category, :integer
    remove_column :students, :category_count, :string

    add_column :students, :sanctioned_intakes, :text, array: true, default: []
    add_column :students, :admitteds, :text, array: true, default: []
    add_column :students, :categories, :text, array: true, default: []
    
    add_column :students, :gm_seat, :string
    add_column :students, :st_seat, :string
    add_column :students, :sc_seat, :string
    add_column :students, :cat_1_seat, :string
    add_column :students, :cat_2_seat, :string
    add_column :students, :obc_seat, :string
    add_column :students, :other_seat, :string
  end
end
