class RenameColsForStudents < ActiveRecord::Migration[7.0]
  def change
    rename_column :students, :sanctioned_intake_field, :sanctioned_intake_mgmt
    rename_column :students, :admitted_field, :admitted_mgmt
    add_column :students, :admitted_govt, :string
    add_column :students, :sanctioned_intake_govt, :string
  end
end
