class CreateDepartments < ActiveRecord::Migration[7.0]
  def change
    create_table :departments do |t|
      t.integer :faculties_count, default: 0
      t.integer :students_count, default: 0
      t.belongs_to :program, index: true
      t.belongs_to :subject, index: true
      t.belongs_to :department_list, index: true
      t.timestamps
    end
  end
end
