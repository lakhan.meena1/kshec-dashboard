class AddNewColsToStudents < ActiveRecord::Migration[7.0]
  def change
    add_column :students, :sanctioned_intake, :integer
    add_column :students, :admitted, :integer
  end
end
