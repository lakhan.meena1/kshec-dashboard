class CreateAuthorities < ActiveRecord::Migration[7.0]
  def change
    create_table :authorities do |t|
      t.string :name
      t.string :qualification
      t.string :date_of_appointment
      t.string :email, index: true
      t.string :contact_number
      t.belongs_to :designation, index: true
      t.timestamps
    end
  end
end
