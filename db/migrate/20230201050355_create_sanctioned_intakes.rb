class CreateSanctionedIntakes < ActiveRecord::Migration[7.0]
  def change
    create_table :sanctioned_intakes do |t|
      t.integer :intake_type
      t.integer :sanctioned_mgmt_seat
      t.integer :sanctioned_govt_seat
      t.references :student, index: true
      t.timestamps
    end
  end
end
