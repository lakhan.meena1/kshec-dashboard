class CreateProgrammesOffereds < ActiveRecord::Migration[7.0]
  def change
    create_table :programmes_offereds do |t|
      t.belongs_to :basic_information, index: true
      t.belongs_to :department_list, index: true
      t.belongs_to :program, index: true
      t.timestamps
    end
  end
end
