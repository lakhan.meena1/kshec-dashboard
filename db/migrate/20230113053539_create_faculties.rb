class CreateFaculties < ActiveRecord::Migration[7.0]
  def change
    create_table :faculties do |t|
      t.string :name
      t.string :resume
      t.string :date_of_joining
      t.string :qualification
      t.string :total_yrs_of_exp
      t.belongs_to :faculty_designation, index: true
      t.belongs_to :department, index: true
      t.timestamps
    end
  end
end
