class CreateBasicInformations < ActiveRecord::Migration[7.0]
  def change
    create_table :basic_informations do |t|
      t.string :university_name
      t.string :date_of_establishment
      t.string :date_of_operationalization
      t.string :address
      t.integer :nirf_rank
      t.integer :total_stduents, default: 0
      t.integer :total_faculties, default: 0
      t.timestamps
    end
  end
end
