class CreateDepartmentLists < ActiveRecord::Migration[7.0]
  def change
    create_table :department_lists do |t|
      t.string :name
      t.integer :position
      t.timestamps
    end
  end
end
