class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.belongs_to :department_list, index: true
      t.string :semester
      t.string :event_file
      t.timestamps
    end
  end
end
