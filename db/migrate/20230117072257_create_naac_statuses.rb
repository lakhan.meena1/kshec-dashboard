class CreateNaacStatuses < ActiveRecord::Migration[7.0]
  def change
    create_table :naac_statuses do |t|
      t.string :grade
      t.string :valid_upto
      t.string :valid_from
      t.belongs_to :basic_information, index: true
      t.timestamps
    end
  end
end
