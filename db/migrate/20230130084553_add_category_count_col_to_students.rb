class AddCategoryCountColToStudents < ActiveRecord::Migration[7.0]
  def change
    add_column :students, :category_count, :string
  end
end
