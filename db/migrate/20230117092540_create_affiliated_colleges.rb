class CreateAffiliatedColleges < ActiveRecord::Migration[7.0]
  def change
    create_table :affiliated_colleges do |t|
      t.belongs_to :basic_information, index: true
      t.belongs_to :infrastructure, index: true
      t.timestamps
    end
  end
end
