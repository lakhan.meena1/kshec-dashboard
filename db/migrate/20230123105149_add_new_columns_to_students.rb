class AddNewColumnsToStudents < ActiveRecord::Migration[7.0]
  def change
    add_column :students, :sanctioned_intake_field, :string
    add_column :students, :admitted_field, :string
    add_column :students, :admitted_year, :string
  end
end
