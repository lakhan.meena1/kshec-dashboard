class CreateStudents < ActiveRecord::Migration[7.0]
  def change
    create_table :students do |t|
      t.integer :total_stduents, default: 0
      t.integer :male, default: 0
      t.integer :female, default: 0
      t.integer :physical_challenged, default: 0
      t.integer :third_gender, default: 0
      t.integer :within_state , default: 0
      t.integer :outside_state , default: 0
      t.integer :foreign_student , default: 0
      t.integer :category
      t.belongs_to :department, index: true
      t.timestamps
    end
  end
end
