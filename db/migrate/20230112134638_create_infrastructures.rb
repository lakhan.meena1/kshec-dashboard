class CreateInfrastructures < ActiveRecord::Migration[7.0]
  def change
    create_table :infrastructures do |t|
      t.string :name
      t.integer :campus
      t.integer :land_availability, default: 0
      t.integer :no_of_classes, default: 0
      t.integer :no_of_labs, default: 0
      t.integer :total_acres, default: 0
      t.timestamps
    end
  end
end
