class CreateLibraries < ActiveRecord::Migration[7.0]
  def change
    create_table :libraries do |t|
      t.string :name
      t.integer :total_books, default: 0
      t.integer :total_ebooks, default: 0
      t.integer :total_journals, default: 0
      t.integer :total_ejournals, default: 0
      t.belongs_to :infrastructure
      t.timestamps
    end
  end
end
