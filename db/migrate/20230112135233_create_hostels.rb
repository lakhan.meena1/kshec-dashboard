class CreateHostels < ActiveRecord::Migration[7.0]
  def change
    create_table :hostels do |t|
      t.integer :hostel_type
      t.string :name
      t.integer :total_boys, default: 0
      t.integer :total_girls, default: 0
      t.belongs_to :infrastructure, index: true
      t.timestamps
    end
  end
end
