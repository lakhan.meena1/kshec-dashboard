class CreateIctInfrastructures < ActiveRecord::Migration[7.0]
  def change
    create_table :ict_infrastructures do |t|
      t.integer :ict_type
      t.integer :availability
      t.integer :no_of_ict_enabled_classes, default: 0
      t.string :speed
      t.string :service_provider
      t.string :bandwidth
      t.belongs_to :infrastructure
      t.timestamps
    end
  end
end
