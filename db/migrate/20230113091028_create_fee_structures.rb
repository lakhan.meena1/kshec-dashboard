class CreateFeeStructures < ActiveRecord::Migration[7.0]
  def change
    create_table :fee_structures do |t|
      t.decimal :govt_fee, default: 0.0
      t.decimal :management_fee, default: 0.0
      t.belongs_to :department
      t.timestamps
    end
  end
end
