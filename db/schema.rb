# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_01_17_101036) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "affiliated_colleges", force: :cascade do |t|
    t.bigint "basic_information_id"
    t.bigint "infrastructure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["basic_information_id"], name: "index_affiliated_colleges_on_basic_information_id"
    t.index ["infrastructure_id"], name: "index_affiliated_colleges_on_infrastructure_id"
  end

  create_table "authorities", force: :cascade do |t|
    t.string "name"
    t.string "qualification"
    t.string "date_of_appointment"
    t.string "email"
    t.string "contact_number"
    t.bigint "designation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["designation_id"], name: "index_authorities_on_designation_id"
    t.index ["email"], name: "index_authorities_on_email"
  end

  create_table "basic_informations", force: :cascade do |t|
    t.string "university_name"
    t.string "date_of_establishment"
    t.string "date_of_operationalization"
    t.string "address"
    t.integer "nirf_rank"
    t.integer "total_stduents", default: 0
    t.integer "total_faculties", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "department_lists", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.integer "faculties_count", default: 0
    t.integer "students_count", default: 0
    t.bigint "program_id"
    t.bigint "subject_id"
    t.bigint "department_list_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_list_id"], name: "index_departments_on_department_list_id"
    t.index ["program_id"], name: "index_departments_on_program_id"
    t.index ["subject_id"], name: "index_departments_on_subject_id"
  end

  create_table "designations", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.bigint "department_list_id"
    t.string "semester"
    t.string "event_file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_list_id"], name: "index_events_on_department_list_id"
  end

  create_table "faculties", force: :cascade do |t|
    t.string "name"
    t.string "resume"
    t.string "date_of_joining"
    t.string "qualification"
    t.string "total_yrs_of_exp"
    t.bigint "faculty_designation_id"
    t.bigint "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_faculties_on_department_id"
    t.index ["faculty_designation_id"], name: "index_faculties_on_faculty_designation_id"
  end

  create_table "faculty_designations", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fee_structures", force: :cascade do |t|
    t.decimal "govt_fee", default: "0.0"
    t.decimal "management_fee", default: "0.0"
    t.bigint "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_fee_structures_on_department_id"
  end

  create_table "hostels", force: :cascade do |t|
    t.integer "hostel_type"
    t.string "name"
    t.integer "total_boys", default: 0
    t.integer "total_girls", default: 0
    t.bigint "infrastructure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["infrastructure_id"], name: "index_hostels_on_infrastructure_id"
  end

  create_table "ict_infrastructures", force: :cascade do |t|
    t.integer "ict_type"
    t.integer "availability"
    t.integer "no_of_ict_enabled_classes", default: 0
    t.string "speed"
    t.string "service_provider"
    t.string "bandwidth"
    t.bigint "infrastructure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["infrastructure_id"], name: "index_ict_infrastructures_on_infrastructure_id"
  end

  create_table "infrastructures", force: :cascade do |t|
    t.string "name"
    t.integer "campus"
    t.integer "land_availability", default: 0
    t.integer "no_of_classes", default: 0
    t.integer "no_of_labs", default: 0
    t.integer "total_acres", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "libraries", force: :cascade do |t|
    t.string "name"
    t.integer "total_books", default: 0
    t.integer "total_ebooks", default: 0
    t.integer "total_journals", default: 0
    t.integer "total_ejournals", default: 0
    t.bigint "infrastructure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["infrastructure_id"], name: "index_libraries_on_infrastructure_id"
  end

  create_table "naac_statuses", force: :cascade do |t|
    t.string "grade"
    t.string "valid_upto"
    t.string "valid_from"
    t.bigint "basic_information_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["basic_information_id"], name: "index_naac_statuses_on_basic_information_id"
  end

  create_table "programmes_offereds", force: :cascade do |t|
    t.bigint "basic_information_id"
    t.bigint "department_list_id"
    t.bigint "program_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["basic_information_id"], name: "index_programmes_offereds_on_basic_information_id"
    t.index ["department_list_id"], name: "index_programmes_offereds_on_department_list_id"
    t.index ["program_id"], name: "index_programmes_offereds_on_program_id"
  end

  create_table "programs", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.integer "total_stduents", default: 0
    t.integer "male", default: 0
    t.integer "female", default: 0
    t.integer "physical_challenged", default: 0
    t.integer "third_gender", default: 0
    t.integer "within_state", default: 0
    t.integer "outside_state", default: 0
    t.integer "foreign_student", default: 0
    t.integer "category"
    t.bigint "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_students_on_department_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.bigint "program_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["program_id"], name: "index_subjects_on_program_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
