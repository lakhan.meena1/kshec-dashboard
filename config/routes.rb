Rails.application.routes.draw do
  root "authorities#index"
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords'
  }
  
  resources :authorities
  resources :basic_informations
  resources :infrastructures
  resources :events
  resources :departments do
    collection do
      get :get_subjects
    end
  end
end
